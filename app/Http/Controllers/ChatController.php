<?php

namespace App\Http\Controllers;

use App\Models\User;

use Illuminate\Http\Request;
use DB;

class ChatController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $friends=DB::table('friends')
        ->join('users','users.id','=','friends.friendOne')
        ->select('users.name','users.profilePicture','users.id')
        ->where('friends.friendTwo',auth()->user()->id)
        ->get();


        return view('chat')->with('friends' ,$friends);
    }
}
