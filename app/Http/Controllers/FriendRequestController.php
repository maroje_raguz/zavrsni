<?php

namespace App\Http\Controllers;


use App\Models\FriendRequest;
use Illuminate\Http\Request;
use DB;

class FriendRequestController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getFriendRequests()
    {
        $friends=DB::table('friends')
        ->join('users','users.id','=','friends.friendOne')
        ->select('users.name','users.profilePicture','users.id')
        ->where('friends.friendTwo',auth()->user()->id)
        ->get();
        return view('friendRequest')
            ->with('requests',DB::table('friend_requests')
                ->join('users','users.id','=','friend_requests.sender')
                ->select('users.name','users.profilePicture','friend_requests.id' )
                ->where('friend_requests.receiver',auth()->user()->id)
                ->get())
            ->with('friends',$friends)
            ->with('user',DB::table('friend_requests')
                ->join('users','users.id','=','friend_requests.sender')
                ->select('users.id' )
                ->where('friend_requests.receiver',auth()->user()->id)
                ->get());
    }

    public function friendRequestCreate($id)
    {
        FriendRequest::create([
            'sender' => auth()->user()->id,
            'receiver' => $id
        ]);

        return redirect('profile/'.$id);
    }

    public function friendRequestDelete($id)
    {
        FriendRequest::destroy($id);

        return redirect('/');
    }
}
