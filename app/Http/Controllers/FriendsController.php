<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Friends;

class FriendsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getUserFriends()
    {
        return DB::table('friends')
                ->join('users','users.id','=','friends.friendOne')
                ->select('users.name','users.profilePicture','users.id')
                ->where('friends.friendTwo',auth()->user()->id)
                ->get();
    }
    public function friendsCreate($userId)
    {
        Friends::create([
            'friendTwo'=> $userId,
            'friendOne'=> auth()->user()->id
        ]);
        Friends::create([
            'friendOne'=> $userId,
            'friendTwo'=> auth()->user()->id
        ]);

        DB::table('friend_requests')
                ->where('sender','=',$userId)
                ->where('receiver','=',auth()->user()->id)
                ->delete();
        return redirect('profile/'.$userId);
    }

    
    public function friendsDelete($userId)
    {
        DB::table('friends')
                ->where('friendOne','=',$userId)
                ->where('friendTwo','=',auth()->user()->id)
                ->delete();

         DB::table('friends')
                    ->where('friendOne','=',auth()->user()->id)
                    ->where('friendTwo','=',$userId)
                    ->delete();
        return 'delted';
    }
}
