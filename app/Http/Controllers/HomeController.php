<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $friends=DB::table('friends')
        ->join('users','users.id','=','friends.friendOne')
        ->select('users.name','users.profilePicture','users.id')
        ->where('friends.friendTwo',auth()->user()->id)
        ->get();

        $userId= auth()->user()->id;

        return view('home')
                ->with('friends',$friends)
                ->with('posts',DB::select(
                    "select posts.id,posts.userId,posts.description,posts.imageUrl, users.name as user_name, users.profilePicture
                    from posts,users
                    where  posts.userid = users.id  and (posts.userId=$userId or posts.userid in (select friends.friendone from friends where friendTwo=$userId) )
                    group by posts.id
                    order by posts.created_at desc
                    "
                ));
        ;
    }
}
