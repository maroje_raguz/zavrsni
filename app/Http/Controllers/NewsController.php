<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\News;
use DB;

class NewsController extends Controller
{
    

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getNews(){

        $friends=DB::table('friends')
        ->join('users','users.id','=','friends.friendOne')
        ->select('users.name','users.profilePicture','users.id')
        ->where('friends.friendTwo',auth()->user()->id)
        ->get('');

        return view('news')
                ->with('news',News::all())
                ->with('friends', $friends);
    }
}
