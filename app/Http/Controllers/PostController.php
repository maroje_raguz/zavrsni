<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use DB;


class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postCreate(Request $request)
    {
        $request->validate([
            'description' => 'required',
            'imageUrl' => 'mimes:png,jpg,jpeg|max:5048'
        ]);

        if($request->imageUrl == null){
            $newImage = '';
        } else{
            $newImage = uniqid() . '-'.auth()->user()->id. '.' . $request->imageUrl->extension();
        }

        if($request->imageUrl != null){
            $request->imageUrl->move(public_path('images'),$newImage);
        }

        Post::create([
            'userId' => auth()->user()->id,
            'date' => now(),
            'description' => $request->description,
            'imageUrl' => $newImage
        ]);

        return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function postDelete($id)
    {
        post::destroy($id);

        return redirect('/');
    }
}
