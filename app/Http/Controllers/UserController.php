<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\User;


class UserController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function getUser($id)
    {
        $friends=DB::table('friends')
        ->join('users','users.id','=','friends.friendOne')
        ->select('users.name','users.profilePicture','users.id')
        ->where('friends.friendTwo',auth()->user()->id)
        ->get();

        $friendStatus='null';

        $isFriend=DB::table('friends')->where('friendOne',$id)->where('friendTwo',auth()->user()->id);
        $requestSend=DB::table('friend_requests')->where('receiver',$id)->where('sender',auth()->user()->id);

        if($isFriend->first()!=null){
            $friendStatus='friends';
        }else if($requestSend->first()!=null){
            $friendStatus='request';
        }

        return view('profile')
        ->with('user',DB::table('users')
                    ->where('users.id',$id)
                    ->get()
                )
        ->with('friends',$friends)
        ->with('posts',DB::table('posts')
                            ->where('posts.userId',$id)
                            ->orderBy('posts.created_at','desc')
                            ->get()
            )
        ->with('friendStatus',$friendStatus);
         
    }

    public function search($userName){
        $friends=DB::table('friends')
        ->join('users','users.id','=','friends.friendOne')
        ->select('users.name','users.profilePicture','users.id')
        ->where('friends.friendTwo',auth()->user()->id)
        ->get();

        return view('search')
            ->with('users',DB::table('users')
                ->where('users.name','like','%'.$userName.'%')
                ->get())
            ->with('friends',$friends);

    }

    public function editProfile(){
        $friends=DB::table('friends')
        ->join('users','users.id','=','friends.friendOne')
        ->select('users.name','users.profilePicture','users.id')
        ->where('friends.friendTwo',auth()->user()->id)
        ->get();

        return view('editProfile')
            ->with('posts',DB::table('posts')
                        ->where('posts.userId',auth()->user()->id)
                        ->orderBy('posts.created_at','desc')
                        ->get())
            ->with('friends',$friends);
    }




    public function updateUser(Request $request){

        $request->validate([
            'profileImage' => 'mimes:png,jpg,jpeg|max:5048',
            'coverImage' => 'mimes:png,jpg,jpeg|max:5048'
        ]);

         $newCoverImage=auth()->user()->coverPicture;
         $newProfileImage=auth()->user()->profilePicture;

        if($request->profileImage != null){
            $newProfileImage = uniqid() . '-'.auth()->user()->id. '.' . $request->profileImage->extension();
            $request->profileImage->move(public_path('images'),$newProfileImage);
        } 
        if($request->coverImage != null){
            $newCoverImage = uniqid() . '-'.auth()->user()->id. '.' . $request->coverImage->extension();
            $request->coverImage->move(public_path('images'),$newCoverImage);
        }


         
         DB::table('users')
                    ->where('users.id',auth()->user()->id)
                    ->update([
                        'city' => $request->city,
                        'country' => $request->country,
                        'relationship' => $request->relationship,
                        'age' => $request->age,
                        'interestedInto' => $request->interestedInto,
                        'profilePicture' => $newProfileImage,
                        'coverPicture' => $newCoverImage
                    ]);

        return redirect('/profile'.'/'.auth()->user()->id);

    }

    public function searchHelp(Request $request){
        return redirect('search/'.$request->search);
    }

    public function getCurrentUser(){
        return DB::table('users')
        ->where('users.id',auth()->user()->id)
        ->get(['name','id'])
        ->first();
    }

   
}
