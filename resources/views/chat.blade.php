@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-5">
            <div class="sidebarchat">
                <div class="sidebarWrapper">
                    @if($friends->count() > 0)
                    <h4 class="chatWithFriends"><b>Chat with Friends</b></h4>
                    <ul class="sidebarFriendList">
                      @foreach ($friends as $friend)
                      <a href="javascript:void(0);" class="chat-toggle friendsLink" data-id="{{ $friend->id }}" data-user="{{ $friend->name }}" class="friendsLink">
                        <li class="rightbarFriend">
                          <div class="rightbarProfileImgContainer">
                            <img class="rightbarProfileImg" src="{{ asset('images/'.$friend->profilePicture) }}" alt="" />
                          </div>
                          <span><b>{{ $friend->name }}</b></span>
                        </li>
                      </a>
                      @endforeach
                    </ul>
                    @else
                    <p>No friends found</p>
                     @endif
                </div>
              </div>
        </div>
    </div>
    @include('chat-box')
    <input type="hidden" id="current_user" value="{{ Auth::user()->id }}" />
    <input type="hidden" id="pusher_app_key" value="{{ env('PUSHER_APP_KEY') }}" />
    <input type="hidden" id="pusher_cluster" value="{{ env('PUSHER_APP_CLUSTER') }}" />
@stop
@section('script')
    <script src="https://js.pusher.com/4.1/pusher.min.js"></script>
    <script src="{{ asset('js/chat.js') }}"></script>
@stop