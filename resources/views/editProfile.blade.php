@extends('layouts.topbar')

@section('content')
<div class="profile">

  <div class="sidebar">
    <div class="sidebarWrapper">
        <h4 >Online Friends</h4>
        <ul class="sidebarFriendList">
          @foreach ($friends as $friend)
          <a href="/profile/{{ $friend->id }}" class="friendsLink">
            <li class="rightbarFriend">
              <div class="rightbarProfileImgContainer">
                <img class="rightbarProfileImg" src="{{ asset('images/'.$friend->profilePicture) }}" alt="" />
              </div>
              <span><b>{{ $friend->name }}</b></span>
            </li>
          </a>
          @endforeach
        </ul>
    </div>
  </div>

  <div class="profileRight">
    <div class="profileRightTop">
      <div class="profileCover">
        <img
          class="profileCoverImg"
          src="{{asset('images/'.auth()->user()->coverPicture) }}"
          alt=""
        />
        <img
          class="profileUserImg"
          src="{{asset('images/'.auth()->user()->profilePicture) }}"
          alt=""
        />
      </div>
        <div class="profileInfo">
        <h4 class="profileInfoName">{{ auth()->user()->name }}</h4>    
        </div>
    </div>
    <div class="profileRightBottom">
      <div class="feedProfile">
        <div class="feedWrapper">
          <div class="share">
            <div class="shareWrapper">
              <div class="shareTop">
                <img class="shareProfileImg" src="{{asset('images/'.auth()->user()->profilePicture) }}" alt="" />
                <form action="/postCreate" method="POST" enctype="multipart/form-data" >
                  @csrf
                  @method('POST')
                <input
                  placeholder="What's in your mind {{ auth()->user()->name }}?"
                  class="shareInput"
                  name="description"
                />
              </div>
              <hr class="shareHr"/>
              <div class="shareBottom">
                  <div class="shareOptions">
                    <div class="shareOption">
                      
                        <label>              
                          <span class="photoButton">Add Photo</span>
                          <input class="inputImg" type="file" name="imageUrl" >
                          </span>
                        </label>
                    </div>
                  </div>
                 <button class="shareButton" type="submit">Share</button>
                  
              </div>
            </form>
            </div>
          </div>
          @foreach ($posts as $post)
          <div class="post">
            <div class="postWrapper">
              <div class="postTop">
                <div class="postTopLeft">
                  <img
                    class="postProfileImg"
                    src="{{ asset('images/'.auth()->user()->profilePicture )}}"
                    alt=""
                  />
                  <span class="postUsername">
                    <b>{{ auth()->user()->name }}</b>
                  </span>
                  
                </div>
              </div>
              <div class="postCenter">
                <span class="postText">{{ $post->description }}</span>
                <img class="postImg" src="{{ asset('images/'.$post->imageUrl )}}" alt="" />
              </div>
              @if ($post->userId == auth()->user()->id)
              <div class="postBottom">
                <form action="/postDelete/{{ $post->id }}" method="POST" >
                  @csrf
                  @method('delete')
                   <span>
                      <button class="deletePostButton">delete</button>
                   </span>
                </form>
              	</div>
              @endif
            </div>
          </div>
          @endforeach
        </div>
      </div>

      <div class="rightbarProfile">
        <div class="rightbarWrapper">
            <h4 class="rightbarTitle">User information</h4>
            <form action="/editProfile" method="POST" enctype="multipart/form-data">
              @csrf
              @method('put')
            <div class="rightbarInfo">
              <div class="rightbarInfoItem">
                <span class="rightbarInfoKey">City:</span>
                <textarea class="rigthbarInfo" name="city" >{{ auth()->user()->city }}</textarea>
              </div>
              <div class="rightbarInfoItem">
                <span class="rightbarInfoKey">County:</span>
                <textarea class="rigthbarInfo" name="country">{{ auth()->user()->country }}</textarea>
              </div>
              <div class="rightbarInfoItem">
                <span class="rightbarInfoKey">Relationship:</span>
                <textarea class="rigthbarInfo" name="relationship" >{{ auth()->user()->relationship }}</textarea>
              </div>
              <div class="rightbarInfoItem">
                <span class="rightbarInfoKey">Age:</span>
                <textarea class="rigthbarInfo" name="age">{{ auth()->user()->age }}</textarea>
              </div>
              <div class="rightbarInfoItem">
                <span class="rightbarInfoKey">Interested into:</span>
                <textarea class="rigthbarInfo" name="interestedInto" >{{ auth()->user()->interestedInto }}</textarea>
              </div>
            </div>
            <div class="labelEdit">
              <label>              
                <span class="editProfileButton">Change Profile Picture</span>
                <input class="inputImg" type="file" name="profileImage" >
                </span>
              </label>
              <label>              
                <span class="editProfileButton">Change Cover Picture</span>
                <input class="inputImg" type="file" name="coverImage" >
                </span>
              </label>
            </div> 
            <button class="rightbarSaveButton" type="submit">Save changes</button>
          </form>
        </div>
      </div>
    </div>

  </div>
</div>
@endsection