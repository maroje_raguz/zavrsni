@extends('layouts.topbar')

@section('content')
<div class="profile">

    <div class="sidebar">
      <div class="sidebarWrapper">
          <h4 >Friends</h4>
          <ul class="sidebarFriendList">
            @foreach ($friends as $friend)
            <a href="/profile/{{ $friend->id }}" class="friendsLink">
              <li class="rightbarFriend">
                <div class="rightbarProfileImgContainer">
                  <img class="rightbarProfileImg" src="{{ asset('images/'.$friend->profilePicture) }}" alt="" />
                </div>
                <span><b>{{ $friend->name }}</b></span>
              </li>
            </a>
            @endforeach
          </ul>
      </div>
    </div>
  
    <div class="profileRight">
      <div class="profileRightBottom">
        <div class="friendRequest">
            <div class="friendRequestWrapper">
              <ul>
               @foreach ( $requests as $request )
                <li class="friendRequestFriend">
                  <div class="friendRequestImgText">
                  <div class="friendRequestProfileImgContainer">
                    <img
                      class="friendRequestProfileImg"
                      src="{{ asset('images/'.$request->profilePicture )}}"
                      alt=""
                      onclick="location.href='/profile/{{ $request->id }}'"
                    />
                  </div>
                  <span class="friendRequestUsername" onclick="location.href='/profile/{{ $user->first()->id }}'">{{ $request->name }}</span>
                  <span class="friendRequestText">sends you friend request</span>
                  </div>
                  <div class="friendRequestButtons">
                      
                      <form action="addFriend/{{ $user->first()->id }}" method="POST"  enctype="multipart/form-data">
                        @csrf
                        @method('post')
                        <span><button class="friendRequestAcceptButton" type="submit">accept</button></span>
                      </form>
                      <form action="friendRequest/{{ $request->id }}" method="POST"  enctype="multipart/form-data">
                        @csrf
                        @method('delete')
                        <span><button class="friendRequestDeclineButton" type="submit">decline</button></span>
                      </form>
                      
                  </div>
                </li>
               @endforeach
                
              </ul>
              
            </div>
          </div>
      </div>
    </div>
  </div>
@endsection