@extends('layouts.topbar')

@section('content')
<div class="profile">

    <div class="sidebar">
      <div class="sidebarWrapper">
          <h4 ><b>Friends</b></h4>
          <ul class="sidebarFriendList">
            @foreach ($friends as $friend)
            <a href="/profile/{{ $friend->id }}" class="friendsLink">
              <li class="rightbarFriend">
                <div class="rightbarProfileImgContainer">
                  <img class="rightbarProfileImg" src="{{ asset('images/'.$friend->profilePicture) }}" alt="" />
                </div>
                <span><b>{{ $friend->name }}</b></span>
              </li>
            </a>
            @endforeach
          </ul>
      </div>
    </div>
  
    <div class="profileRight">
      <div class="profileRightBottom">
        <div class="feedProfile">
          <div class="feedWrapper">
            <div class="share">
              <div class="shareWrapper">
                <div class="shareTop">
                  <img class="shareProfileImg" src="{{asset('images/'.auth()->user()->profilePicture) }}" alt="" />
                  <form action="/postCreate" method="POST" enctype="multipart/form-data" >
                    @csrf
                    @method('POST')
                  <input
                    placeholder="What's in your mind {{ auth()->user()->name }}?"
                    class="shareInput"
                    name="description"
                  />
                </div>
                <hr class="shareHr"/>
                <div class="shareBottom">
                    <div class="shareOptions">
                      <div class="shareOption">
                        
                          <label>              
                            <span class="photoButton">Add Photo</span>
                            <input class="inputImg" type="file" name="imageUrl" >
                            </span>
                          </label>
                      </div>
                    </div>
                   <button class="shareButton" type="submit">Share</button>
                    
                </div>
              </form>
              </div>
            </div>
            @foreach ($posts as $post)
            <div class="post">
              <div class="postWrapper">
                <div class="postTop">
                  <div class="postTopLeft">
                    <img
                      class="postProfileImg"
                      src="{{ asset('images/'.$post->profilePicture) }}"
                      alt=""
                      onclick="location.href='/profile/{{ $post->userId }}'"
                    />
                    <span class="postUsername" onclick="location.href='/profile/{{ $post->userId }}'">
                      <b>{{ $post->user_name }}</b>
                    </span>
                    
                  </div>
                </div>
                <div class="postCenter">
                  <span class="postText">{{ $post->description }}</span>
                  <img class="postImg" src="{{ asset('images/'. $post->imageUrl) }}" alt="" />
                </div>
                @if ($post->userId == auth()->user()->id)
                <div class="postBottom">
                  <form action="/postDelete/{{ $post->id }}" method="POST" >
                    @csrf
                    @method('delete')
                     <span>
                        <button class="deletePostButton">delete</button>
                     </span>
                  </form>
              </div>
                @endif

              </div>
            </div>
            @endforeach
          </div>
        </div>
  
        
      </div>
  
    </div>
  </div>
@endsection
