<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/chat.css') }}" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>

    <script>
        var base_url = '{{ url("/") }}';
    </script>

    <script src="{{ asset('js/app.js') }}"></script>
</head>
<body>
  <div class="topbarContainer">
    <div class="topbarLeft">
      <span class="logo"><a class="logoLink" href="/">Senior 60+</a></span>
    </div>
    <div class="topbarCenter">
      <form action="/search">
        <input placeholder="Search for friend" class="searchbar" id="myInputID" name="search"/>
        <script>
          document.getElementById("myInputID").addEventListener("keyup", function(event) {
              if (event.keyCode === 13) {
                document.getElementById("myFormID").submit();
              return false;
              }
          });
        </script>
      </form>
       
    </div>
    <div class="topbarRight">
      @guest
          @if (Route::has('login'))
            <div class="topbarLinks">
              <span class="topbarLink">
                <a class="topbarLink" href="/login">
                  login
                </a>
              </span>
            </div>
          @endif

          @if (Route::has('register'))
            <div class="topbarLinks">
              <span class="topbarLink">
                <a class="topbarLink" href="/register">
                  register
                </a>
              </span>
            </div>
          @endif
      @else
        <div class="topbarIcons">
          <div class="topbarLinks">
            <span class="topbarLink">
              <a class="topbarLink" href="/request">
                Friend request
              </a>
            </span>
          </div>
          <div class="topbarLinks">
            <span class="topbarLink">
              <a class="topbarLink" href="/chat">
                Chat
              </a>
            </span>
          </div>
          <div class="topbarLinks">
            <span class="topbarLink">
              <a class="topbarLink" href="/news">
                News
              </a>
            </span>
          </div>
          <div class="topbarLinks">
            <span class="topbarLink">
              <a class="topbarLink" href="/profile/{{ Auth::user()->id }}">
                {{ Auth::user()->name }}
              </a>
            </span>
          </div>
          <div class="topbarLinks">
            <span class="topbarLink">
              <a class="topbarLink" href="{{ route('logout') }}" onclick="event.preventDefault();
              document.getElementById('logout-form').submit();">
                logout
              </a>
            </span>
          </div>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                 @csrf
              </form>
           
        </li>
          
        </div>
        @endguest
      </div>
      
    </div>
    <div id="app">
        <div class="container-fluid">
            @yield('content')
        </div>

        <div id="chat-overlay" class="row"></div>

    </div>

    <input type="hidden" id="current_user" value="{{ \Auth::user()->id }}" />
</body>
</html>