<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#000000" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link
    href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,500&display=swap"
    rel="stylesheet">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div class="topbarContainer">
        <div class="topbarLeft">
          <span class="logo"><a class="logoLink" href="/">Senior 60+</a></span>
        </div>
        <div class="topbarCenter">
          <form action="/search">
            <input placeholder="Search for friend" class="searchbar" id="myInputID" name="search"/>
            <script>
              document.getElementById("myInputID").addEventListener("keyup", function(event) {
                  if (event.keyCode === 13) {
                    document.getElementById("myFormID").submit();
                  return false;
                  }
              });
            </script>
          </form>
           
        </div>
        <div class="topbarRight">
          @guest
              @if (Route::has('login'))
                <div class="topbarLinks">
                  <span class="topbarLink">
                    <a class="topbarLink" href="/login">
                      login
                    </a>
                  </span>
                </div>
              @endif

              @if (Route::has('register'))
                <div class="topbarLinks">
                  <span class="topbarLink">
                    <a class="topbarLink" href="/register">
                      register
                    </a>
                  </span>
                </div>
              @endif
          @else
            <div class="topbarIcons">
              <div class="topbarLinks">
                <span class="topbarLink">
                  <a class="topbarLink" href="/request">
                    Friend request
                  </a>
                </span>
              </div>
              <div class="topbarLinks">
                <span class="topbarLink">
                  <a class="topbarLink" href="/chat">
                    Chat
                  </a>
                </span>
              </div>
              <div class="topbarLinks">
                <span class="topbarLink">
                  <a class="topbarLink" href="/news">
                    News
                  </a>
                </span>
              </div>
              <div class="topbarLinks">
                <span class="topbarLink">
                  <a class="topbarLink" href="/profile/{{ Auth::user()->id }}">
                    {{ Auth::user()->name }}
                  </a>
                </span>
              </div>
              <div class="topbarLinks">
                <span class="topbarLink">
                  <a class="topbarLink" href="{{ route('logout') }}" onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();">
                    logout
                  </a>
                </span>
              </div>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                     @csrf
                  </form>
               
            </li>
              
            </div>
            @endguest
          </div>
          
        </div>
        
        @yield('content')
       
    </div>
</body>
</html>
