@extends('layouts.topbar')

@section('content')
<div class="newsContainer">
    <div class="sidebar">
        <div class="sidebarWrapper">
            <h4 >Friends</h4>
            <ul class="sidebarFriendList">
                @foreach ($friends as $friend)
                <a href="/profile/{{ $friend->id }}" class="friendsLink">
                  <li class="rightbarFriend">
                    <div class="rightbarProfileImgContainer">
                      <img class="rightbarProfileImg" src="{{ asset('images/'.$friend->profilePicture) }}" alt="" />
                    </div>
                    <span><b>{{ $friend->name }}</b></span>
                  </li>
                </a>
                @endforeach
            </ul>
        </div>
    </div>
    <div class="allNews">
      @foreach ($news as $allNews )
      <div class="news">
        <div class="newsWrapper">
          <div class="cardN cardHover">
            <div class="newsImageDiv">
              <img class="newsImage" src="{{ $allNews->imageUrl }}" alt="" />
            </div>
            <div class="newsContent">
              <div class="newsTitleDiv">
                <h3 class="newsTitle">{{ $allNews->title }}</h3>
              </div>
              <div class="newsBody">
                <p class="newsText">
                 {{ $allNews->description }}
                </p>
              </div>
            </div>
            <div class="newsButton">
              <button class="viewMoreButton">
                <a class="newsLink" href="{{ $allNews->viewMoreUrl }}">VIEW MORE</a>
              </button>
            </div>
          </div>
        </div>
      </div>
      @endforeach  
    </div>  
      
</div>

@endsection