@extends('layouts.topbar')

@section('content')
<div class="profile">

  <div class="sidebar">
    <div class="sidebarWrapper">
        <h4 >Friends</h4>
        <ul class="sidebarFriendList">
          @foreach ($friends as $friend)
          <a href="/profile/{{ $friend->id }}" class="friendsLink">
            <li class="rightbarFriend">
              <div class="rightbarProfileImgContainer">
                <img class="rightbarProfileImg" src="{{ asset('images/'.$friend->profilePicture) }}" alt="" />
              </div>
              <span><b>{{ $friend->name }}</b></span>
            </li>
          </a>
          @endforeach
        </ul>
    </div>
  </div>

  <div class="profileRight">
    <div class="profileRightTop">
      <div class="profileCover">
        <img
          class="profileCoverImg"
          src="{{ asset('images/'.$user->first()->coverPicture) }}"
          alt=""
        />
        <img
          class="profileUserImg"
          src="{{ asset('images/'.$user->first()->profilePicture) }}"
          alt=""
        />
      </div>
        <div class="profileInfo">
        <h4 class="profileInfoName">{{ $user->first()->name }}</h4>
        <span>
          @if ($user->first()->id == auth()->user()->id)
            <button class="editProfileButton" onclick="location.href='/editProfile'">Edit profile</button>
          @elseif ($friendStatus=='friends')
            <button class="friendRequestButton">Friends</button>
          @elseif ($friendStatus=='request')  
            <button class="friendRequestButton">Friend request send</button>
          @else
          <form action="/sendRequest/{{ $user->first()->id }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('POST')
            <button class="friendRequestButton" type="submit">Add Friend</button>
          </form>
          
          @endif
        </span>    
        </div>
    </div>
    <div class="profileRightBottom">
      <div class="feedProfile">
        <div class="feedWrapper">
          @if ($user->first()->id == auth()->user()->id)
          <div class="share">
            <div class="shareWrapper">
              <div class="shareTop">
                <img class="shareProfileImg" src="{{ asset('images/'.auth()->user()->profilePicture) }}" alt="" />
                <form action="/postCreate" method="POST" enctype="multipart/form-data" >
                  @csrf
                  @method('POST')
                <input
                  placeholder="What's in your mind {{ auth()->user()->name }}?"
                  class="shareInput"
                  name="description"
                />
              </div>
              <hr class="shareHr"/>
              <div class="shareBottom">
                  <div class="shareOptions">
                    <div class="shareOption">
                      
                        <label>              
                          <span class="photoButton">Add Photo</span>
                          <input class="inputImg" type="file" name="imageUrl" >
                          </span>
                        </label>
                    </div>
                  </div>
                 <button class="shareButton" type="submit">Share</button>
                  
              </div>
            </form>
            </div>
          </div>
          @endif
          @if ($friendStatus=='friends' || $user->first()->id==auth()->user()->id)
          @foreach ($posts as $post)
          <div class="post">
            <div class="postWrapper">
              <div class="postTop">
                <div class="postTopLeft">
                  <img
                    class="postProfileImg"
                    src="{{ asset('images/'.$user->first()->profilePicture) }}"
                    alt=""
                  />
                  <span class="postUsername">
                    <b>{{ $user->first()->name }}</b>
                  </span>
                  
                </div>
              </div>
              <div class="postCenter">
                <span class="postText">{{ $post->description }}</span>
                <img class="postImg" src="{{ asset('images/'. $post->imageUrl) }}" alt="" />
              </div>
              @if ($post->userId == auth()->user()->id)
              <div class="postBottom">
                <form action="/postDelete/{{ $post->id }}" method="POST" >
                  @csrf
                  @method('delete')
                   <span>
                      <button class="deletePostButton">delete</button>
                   </span>
                </form>
               
              	</div>
              @endif
            </div>
          </div>
          @endforeach

        </div>
      </div>
      <div className="rightbarProfile">
        <div className="rightbarWrapper">
          <h4 class="rightbarTitle righbarTitleMargin">User information</h4>
          <div class="rightbarInfo">
              <div class="rightbarInfoItem">
                <span class="rightbarInfoKey">City:</span>
                <span class="rightbarInfoValue">{{ $user->first()->city }}</span>
              </div>
              <div class="rightbarInfoItem">
                <span class="rightbarInfoKey">Country:</span>
                <span class="rightbarInfoValue">{{ $user->first()->country }}</span>
              </div>
              <div class="rightbarInfoItem">
                <span class="rightbarInfoKey">Relationship:</span>
                <span class="rightbarInfoValue">{{ $user->first()->relationship }}</span>
              </div>
              <div class="rightbarInfoItem">
                <span class="rightbarInfoKey">Age:</span>
                <span class="rightbarInfoValue">{{ $user->first()->age }}</span>
              </div>
              <div class="rightbarInfoItem">
                <span class="rightbarInfoKey">Interested into:</span>
                <span class="rightbarInfoValue">{{ $user->first()->interestedInto }}</span>
              </div>
          </div>
        </div>
      </div>
      @endif
    </div>

  </div>
</div>
@endsection