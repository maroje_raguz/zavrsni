@extends('layouts.topbar')

@section('content')
<div class="profile">

    <div class="sidebar">
      <div class="sidebarWrapper">
          <h4 >Friends</h4>
          <ul class="sidebarFriendList">
            @foreach ($friends as $friend)
            <a href="/profile/{{ $friend->id }}" class="friendsLink">
              <li class="rightbarFriend">
                <div class="rightbarProfileImgContainer">
                  <img class="rightbarProfileImg" src="{{ asset('images/'.$friend->profilePicture) }}" alt="" />
                </div>
                <span><b>{{ $friend->name }}</b></span>
              </li>
            </a>
            @endforeach
          </ul>
      </div>
    </div>
  
    <div class="profileRight">
      <div class="profileRightBottom">
        <div class="search">
            <div class="searchWrapper">
              <ul>
                @foreach ($users as $user )
                  
                <li class="searchContainer">
                  <div class="searchProfileImgContainer">
                    <img
                      class="searchProfileImg"
                      src="{{ asset('images/'.$user->profilePicture) }}"
                      alt=""
                      onclick="location.href='/profile/{{ $user->id }}'"
                    />
                    <span class="searchUsername" onclick="location.href='/profile/{{ $user->id }}'">{{ $user->name }} </span>
                  </div>
                  <div class="searchProfileButton">
                    <button class="searchVisitProfileButton" onclick="location.href='/profile/{{ $user->id }}'">
                      visit profile
                    </button>
                  </div>
                </li>
                @endforeach
              </ul>
            </div>
          </div>
    </div>
  </div>
@endsection