<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\FriendRequestController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\FriendsController;
use App\Http\Controllers\MessagesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


    Route::get('/home', function () {
        return redirect('/');
    });





Auth::routes();

Route::get('/', [HomeController::class, 'index'])->name('home');

Route::get('/news',[NewsController::class,'getNews']);

Route::get('/profile/{id}',[UserController::class,'getUser']);

Route::get('/editProfile',[UserController::class,'editProfile']);

Route::put('/editProfile',[UserController::class,'updateUser']);


Route::get('/request',[FriendRequestController::class,'getFriendRequests']);

Route::get('/search/{username}',[UserController::class,'search']);

Route::get('/search',[UserController::class,'searchHelp']);

Route::post('/postCreate',[PostController::class,'postCreate']);

Route::delete('/postDelete/{id}',[PostController::class,'postDelete']);

Route::delete('friendRequest/{id}',[FriendRequestController::class,'friendRequestDelete']);

Route::post('addFriend/{id}',[FriendsController::class,'friendsCreate']);

Route::post('sendRequest/{id}',[FriendRequestController::class,'friendRequestCreate']);

Route::get('getCurrentUser', [UserController::class,'getCurrentUser']);


Route::get('/chat', [App\Http\Controllers\ChatController::class, 'index']);

Route::get('/load-latest-messages', [MessagesController::class, 'getLoadLatestMessages']);

Route::post('/send', [MessagesController::class, 'postSendMessage']);

Route::get('/fetch-old-messages', [MessagesController::class, 'getOldMessages']);

Route::get('/emit', function () {
   \App\Events\MessageSent::broadcast(\App\Models\User::find(1));
});